<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->date('date_from');
            $table->date('date_to')->nullable();
            $table->enum('requested',['vacation','sick','other'])->nullable();
            $table->string('comment',160)->nullable();
            $table->boolean('approved')->nullable();
            $table->date('approved_date')->nullable();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->string('approved_note',160)->nullable();
            $table->timestamp('added_on');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('approved_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Project;

class CreateProjectDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_data', function (Blueprint $table) {
	        $table->smallInteger('project_id')->unsigned()->primary();
            $table->string('description',300)->nullable();
            $table->string('status',300)->nullable();
            $table->string('type',50)->nullable();
            $table->string('url',100)->nullable();
            $table->smallInteger('budgeted')->default(0);
            $table->date('deadline')->nullable();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_data');
    }
}

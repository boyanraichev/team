<?php 
	
return [
	/*
	* App version
	*/
	'version' => '0.6',
	
	/*
	* Reporting email
	*/
	'reporting_emails' => [
		'aboyadzhiev@bulmar.com',
		'julien.romagnoli@akktis.com', 
		'julie.brossy@akktis.com', 
		'boyan.raichev@akktis.com',
	],
	
	'vacations_approve_emails' => [
		'julien.romagnoli@akktis.com',
		'julie.brossy@akktis.com',
	],
];
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Team\Repositories\CalendarManage;
use App\Mail\VacationReporting;
use Illuminate\Support\Facades\Mail;

class SendMonthlyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:monthly {--start=} {--end=} {--email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send monthly vacations report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = $this->option('start');
        $end = $this->option('end');
        $email = $this->option('email');
        
        if (empty($email)) {
            $emails = config('team.reporting_emails');
        } else {
            $emails = [$email];
        }
        
        if (empty($start)) {
	        $dateStart = new \DateTime('today');
			$dateStart->modify( 'first day of this month' );
			$start = $dateStart->format('Y-m-d');
        }
        
        if (empty($end)) {
			$dateEnd = new \DateTime('today');
			$dateEnd->modify( 'last day of this month' );
			$end = $dateEnd->format('Y-m-d');
        }

        // get the data...
        $users = CalendarManage::getVacations($start,$end,'all');
        
        foreach($emails as $email) {
            Mail::to($email)->send(new VacationReporting($users));
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Team\Repositories\CalendarManage;
use App\Mail\VacationRequestsList;
use Illuminate\Support\Facades\Mail;

class SendDailyRequestVacationsReport extends Command
{
   /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'requests:list {--email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily vacations requests list to approve';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->option('email');
        
        if (empty($email)) {
            $emails = config('team.vacations_approve_emails');
        } else {
            $emails = [$email];
        }        

        // get the data...
        $requests = CalendarManage::getRequestsVacations();
        $countRequests = count($requests['requests']);
       
        if($countRequests>0) {
            foreach($emails as $email) {
                Mail::to($email)->send(new VacationRequestsList($requests));
            }
        }
    }
}

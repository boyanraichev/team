<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App\User;
use App\Team\Repositories\TeamAuth;

class TeamAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
		try {
			
			$user = Auth::user();
			
			$authorise = TeamAuth::checkAuthority($user);
			
			if (!$authorise) {
				throw new \Exception('No authority');
			}
			
			return $next($request);
			
		} catch(\Exception $e) {
			
			return redirect('/')->with('error', 'Oh la la.');
			
		}
		
        
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use \Datetime;

use Auth;
use App\Models\User;
use App\Models\Calendar;
use App\Models\Holiday;
use App\Models\Request as AwayRequest;
use App\Http\Requests\CalendarSaveRequest; 
use Illuminate\Support\Facades\DB;

use Session;

use App\Http\Requests\CalendarSaveAway;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($from = 'today',$length = 5, $move = null)
    {
		$user = Auth::user();
		
		$calendar = [
			'length' => $length,
			'user' => $user->id,
		];
		
        $date = new \DateTime($from);
        if ($from=='today') {
	        $calendar['today'] = $date->format('Y-m-d');
        } else {
	    	$dateToday = new \DateTime('today');
	    	$calendar['today'] = $dateToday->format('Y-m-d');
        }
        
        if ($length == 5 OR $length == 7) {
	        $week = $date->format('Y-m-d');
	    	switch ($move) {
		    	case 'left':
			        $date->modify('Monday previous week '.$week);
			        break;
		    	case 'right':
			        $date->modify('Monday next week '.$week);
			        break;	
		    	default:
			        $date->modify('Monday this week '.$week);
			        break;			        		        
	    	}
	    } else {
		    switch ($move) {
		    	case 'left':
			        $date->modify('-'.$length.' days');
			        break;
		    	case 'right':
			        $date->modify('+'.$length.' days');
			        break;	
		    	default:
			        break;			        		        
	    	}
	    }
	    
		$calendar['start'] = $date->format('Y-m-d');
		$calendar_start = clone $date;
		
		$calendar['users'] = User::getTeam($user)->select()->get();
        $users = $calendar['users']->pluck('id')->toArray();
        
		if ( Session::get('team_admin') ) {
			$calendar['admin'] = true;
			$users_requests = $calendar['users']->pluck('id')->toArray();
	    } else {
			$calendar['admin'] = false;
			$users_requests = [$user->id];
	    }
	    
        for($d=1;$d<=$length;$d++) {
	        $calendar['calendar'][$date->format('Y-m-d')] = [
		        'date' => $date->format('Y-m-d'),
		        'weekday' => $date->format('l'),
		        'day' => $date->format('d/m'),
		        'users' => array_fill_keys($users, ['tasks'=>[]]),
	        ];
	        $date->modify('+1 day');
        }
        $calendar['end'] = $date->format('Y-m-d');
		$calendar_end = clone $date;
		$calendar_end->modify('-1 day');

	    $holidays = Holiday::period($calendar['start'],$calendar['end'])->get();
	    if (count($holidays)>0) {
		    foreach($holidays as $holiday) {
			    if (isset($calendar['calendar'][$holiday->date])) {
				    $calendar['calendar'][$holiday->date]['holiday'] = true;   
			    }
		    }
		}
		

        $tasks = Calendar::dateRange($calendar['start'],$calendar['end'])
        	->whereIn('user_id', $users)
        	->with(['project'])
        	->orderBy('date','asc')
			->get();
        
        foreach($tasks as $task) {
	        $task_to_add = [
		        'id' => $task->id,
		        'project_id' => $task->project_id,
		        'name' => '',
		        'client_name' => '',
		        'length' => $task->length,
		        'away' => $task->away,
		        'color' => '',
	        ];
	        if ($task->project) {
		        $task_to_add['name'] = $task->project->name;
		        $task_to_add['client_name'] = $task->project->client->name;
		        $task_to_add['color'] = $task->project->color;
	        }
	        $calendar['calendar'][$task['date']]['users'][$task['user_id']]['tasks'][] = $task_to_add;
		}
		
		$requests = AwayRequest::dateRange($calendar['start'],$calendar['end'])
			->whereIn('user_id', $users_requests)
			->whereNull('approved')
        	->orderBy('date_from','asc')
			->get();

		// $hasRequests = count($requests) > 0;
			foreach($requests as $request) {
				$start = new \DateTime($request->date_from);
				if($request->date_to != null) {
					$end = new \DateTime($request->date_to);
				}
				else {
					$end = new \DateTime($request->date_from);
				}

				if ($end > $calendar_end) {
					$end = clone $calendar_end;
				}
				if ($start < $calendar_start) {
					$start = clone $calendar_start;
				}
				for($i = clone $start; $i <= $end; $i->modify('+1 day')) 
				{
					$request_to_add = [
						'id' => $request->id,
						'date' => $i->format("Y-m-d"),
						'date_from' => $request->date_from,
						'date_to' => $request->date_to,
						'user_id' => $request->user_id,
						'length' => $request->length,
						'away' => $request->requested,
						'comment' => $request->comment,
					];
					$calendar['calendar'][$request_to_add['date']]['users'][$request_to_add['user_id']]['requests'][] = $request_to_add;
					// echo $i->format("Y-m-d")."is not holiday";
				}
			}
			
        // echo '<pre>';var_dump($calendar['calendar']); die();
		// var_dump($calendar['calendar']);
		// echo '</pre>';
		// die;
		$html = view(
			'layouts.calendar', $calendar
			)->render();
		
        
        return response()->json([
	        'start' => $calendar['start'],
	        'end' => $calendar['end'],
			'calendar' => $calendar['calendar'],
	        'html' => $html,
        ]);
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRequest(CalendarSaveRequest $request)
    {
	    
        $result = [
        	'result' => false,
        	'message' => 'Could not save to calendar',
        	'id' => null,
		];
		
		$user = Auth::user();

		$now = new \DateTime();
		$start = new \DateTime($request->date_from);
		$end = new \DateTime($request->date_to);
		$length = ( !empty($request->half) ? 0.5 : 1 );

		$awayRequest = AwayRequest::updateOrCreate(
			[
				'user_id' => $user->id,
				'date_from' => $request->date_from,
				'date_to' => $request->date_to
			],
			[
			'date_from' => $request->date_from,
			'date_to' => $request->date_to,
			'length' => $length,
			'user_id' => $user->id,
			'requested' => $request->requested,
			'comment' => $request->comment,
			'approved' => null,
			'approved_date' => null,
			'approved_by' => null,
			'approved_note' => null,
		]);
		
		if ($awayRequest) {
			
			$result = [
				'result' => true,
				'message' => 'The request has been submitted successfully.',
				'id' => $awayRequest->id,
			];
	        
		}
        
		return response()->json($result);
    }

	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
}

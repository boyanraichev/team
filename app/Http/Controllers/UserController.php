<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

	/**
     * Display the user stats.
     *
     * @return \Illuminate\Http\Response
     */
    public function stats()
    {
        $user = Auth::user();
		$today = new \DateTime();
		        
        $user->with(['employee','calendars'=>function($query) use ($today){
	            $query->whereYear('date',$today->format('Y'));
	        }]);

       	$start = new \DateTime($user->employee->start);
       	if ($start->format('Y') < $today->format('Y')) {
            $allowedVacations = $user->employee->vacations_extra + $user->employee->vacations_contract;
       	} else {
           	$monthsIn = 12 - ( $start->format('m') - 1 );
	       	$allowedVacations = round(($user->employee->vacations_extra + ( ( $user->employee->vacations_contract / 12 ) * $monthsIn) ),0);
       	}

       	$vacationsTaken =  $user->calendars()->notInWorkDays('vacation');
       	$sickTaken = $user->calendars()->notInWorkDays('sick');
       	$otherTaken = $user->calendars()->notInWorkDays('other');

        return view('layouts.team.users_stats',[
            'user' =>$user, 
            'allowedVacations' => $allowedVacations,
            'vacations'=>$vacationsTaken,
            'sick' =>$sickTaken, 
            'other'=>$otherTaken
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

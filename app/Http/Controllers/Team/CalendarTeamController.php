<?php

namespace App\Http\Controllers\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\Project;
use App\Models\User;
use App\Models\Calendar;

use App\Team\Helpers\ColorsHelper;
use App\Team\Repositories\CalendarManage;
use App\Mail\VacationReporting;
use App\Http\Requests\CalendarSave;
use App\Http\Requests\CalendarSaveAway;
use App\Models\Request as AwayRequest;
use Session;

class CalendarTeamController extends Controller
{
    /**
     * Show the application dashboard for team admin
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {   
	    
        $user = Auth::user();
		
		$users = User::getTeam($user)->get();
		
		$length = 5;
		    
	    if ( Session::get('team_admin') ) {	
		    
		    // get all active projects
	        $projects = Project::isActive()->get();
	
	        $requests = AwayRequest::select()
				->whereNull('approved')
				->get();

			$hasRequests = count($requests) > 0;
			
	        return view('layouts.team.planner', [ 
		        'length' => $length,
	        	'projects' => $projects, 
	        	'users' => $users,
	            'user' => $user,
	            'hasRequests' => $hasRequests,
	        ]);		    
		    
		} else {
		
			return view('layouts.team.planner', [ 
		        'length' => $length,
	        	'projects' => [], 
	        	'users' => $users,
	            'user' => $user,
	            'hasRequests' => false,
	        ]);	
/*
	        return view('home', [ 
		        'length' => $length,
	        	'users' => $users,
	        	'user' => $user,
	        ]);
		
*/
		}	        
                
    }

    /**
     * Show the vacations report
     *
     * @return \Illuminate\Http\Response
     */
    public function vacationsReport($start=null,$end=null,$id='all') {  

        $users = CalendarManage::getVacations($start,$end,$id);
        return new VacationReporting($users);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CalendarSave $request)
    {
        $result = [
        	'result' => false,
        	'message' => 'Could not save to calendar',
        	'id' => null,
        ];
        
        $length = $request->input('length');
        // check length of other projects?
        
        $project = Project::find($request->project_id);
		if ($project) {
			
			 $task = Calendar::create([
	            'date' => $request->date,
	            'user_id' => $request->user_id,
	            'client_id' => $project->client_id,
	            'project_id' => $project->id,            
	            'length' => $length,
	            'away' => null,
	        ]);
	        
	        if ($task) {
		        $result = [
		        	'result' => true,
		        	'message' => 'Saved',
		        	'id' => $task->id,
		        ];	        
	        }
			
		}
        
		return response()->json($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAway(CalendarSaveAway $request)
    {
        $result = [
        	'result' => false,
        	'message' => 'Could not save to calendar',
        	'id' => null,
        ];
        
        if (empty($request->away)) {
	        $result['message'] = 'Error - missing information.';
	        return response()->json($result);
        }
       
        $user = Auth::user();
        
        // check length of other projects?
        
        $task = Calendar::updateOrCreate([
            'date' => $request->date,
            'user_id' => $request->user_id,
            'away' => $request->away],
            ['project_id' => null,
            'client_id' => null,       
            'length' => $request->length,
            'comment' => $request->comment,
        ]);
        
        if ($task) {
            $result = [
                'result' => true,
                'message' => 'Saved',
                'id' => $task->id,
            ];
        }
        
		return response()->json($result);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CalendarSave $request, $id)
    {
        $result = [
        	'result' => false,
        	'message' => 'Could not save to calendar',
        	'id' => null,
        ];
        
        $calendar = Calendar::find($id);
        
        if ($calendar) {
	        
	        $calendar->length = $request->length;
// 	        $calendar->comment = $request->comment;
	        $calendar->save();
	        
	        $result = [
	        	'result' => true,
	        	'message' => 'Saved',
	        	'id' => $calendar->id,
	        ];	        

        }
        
		return response()->json($result);
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAway(CalendarSaveAway $request, $id)
    {
        $result = [
        	'result' => false,
        	'message' => 'Could not save',
        	'id' => null,
        ];
        
        $calendar = Calendar::find($id);
        
        if ($calendar) {
	        
	        $user = Auth::user();
	        if ($user->id != $calendar->user_id AND !Session::get('team_admin')) {
		        $result['message'] = 'You can only add to your own calendar.';
		        return response()->json($result);
	        }
	        
	        $calendar->comment = $request->comment;
	        $calendar->save();
	        
	        $result = [
	        	'result' => true,
	        	'message' => 'Saved',
	        	'id' => $calendar->id,
	        ];	        

        }
        
		return response()->json($result);
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = [
        	'result' => false,
        	'message' => 'Could not delete calendar',
        	'id' => null,
        ];
        
        $calendar = Calendar::find($id);
        
        if ($calendar) {
	        
	        $result = [
	        	'result' => true,
	        	'message' => 'Deleted',
	        	'id' => $calendar->id,
	        ];	        
	        $calendar->delete();

        }
        
		return response()->json($result);
    }
        

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAway($id)
    {
        $result = [
        	'result' => false,
        	'message' => 'Could not delete',
        	'id' => null,
        ];
        
        $calendar = Calendar::find($id);
        
        if ($calendar) {
	        
	        if (empty($calendar->away)) {
		        $result['message'] = 'You can only delete away days from your calendar.';
		        return response()->json($result);
	        }
	        
	        $user = Auth::user();
	        if ($user->id != $calendar->user_id AND !Session::get('team_admin')) {
		        $result['message'] = 'You can only edit your own calendar.';
		        return response()->json($result);
	        }
	        
	        $today = new \DateTime('today');
	        $date = new \DateTime($calendar->date);
	        if ($today > $date AND !Session::get('team_admin')) {
		        $result['message'] = 'You can not delete events that have already passed.';
		        return response()->json($result);
	        }
	        
	        $result = [
	        	'result' => true,
	        	'message' => 'Deleted',
	        	'id' => $calendar->id,
	        ];	        
	        $calendar->delete();

        }
        
		return response()->json($result);
    }
        
}

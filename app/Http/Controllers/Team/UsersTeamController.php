<?php

namespace App\Http\Controllers\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Vacation;
use App\Models\Employee;

use App\Http\Requests\UserSave;

use App\Team\Repositories\UserRegister;

class UsersTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('layouts.team.users', [ 'users' => $users ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserSave $request, UserRegister $registrar) {
	    
		$user = $registrar->register($request,true);
		
	    if ($user) {   
			return redirect()->route('admin:users:list')->with('success', 'User „'.$user->firstname.' '.$user->lastname.'“ was created!');
		} 
		
		return redirect()->route('admin:users:list')->with('error', 'Could not create user!');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $today = new \DateTime();
	    
        $user = User::query()
        	->with(['employee','calendars'=>function($query) use ($today){
	            $query->whereYear('date',$today->format('Y'));
	        }])
        	->find($id);

		
       	$start = new \DateTime($user->employee->start);
       	if ($start->format('Y') < $today->format('Y')) {
            $allowedVacations = $user->employee->vacations_extra + $user->employee->vacations_contract;
       	} else {
           	$monthsIn = 12 - ( $start->format('m') - 1 );
	       	$allowedVacations = round(($user->employee->vacations_extra + ( ( $user->employee->vacations_contract / 12 ) * $monthsIn) ),0);
       	}

       	$vacationsTaken =  $user->calendars()->notInWorkDays('vacation');
       	$sickTaken = $user->calendars()->notInWorkDays('sick');
       	$otherTaken = $user->calendars()->notInWorkDays('other');

        return view('layouts.team.users_stats',[
            'user' =>$user, 
            'allowedVacations' => $allowedVacations,
            'vacations'=>$vacationsTaken,
            'sick' =>$sickTaken, 
            'other'=>$otherTaken
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if ($user) {   
            return view('layouts.team.user_edit',['user'=>$user]);
        }
        return redirect()->route('admin:users:list')->with('error', 'No such user!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserSave $request, $id)
    {   

        $user = User::find($id);
		
		$admin = $request->input('admin');
		if (empty($admin)) {
			$admin = 0;
		}
		$member = $request->input('member');
		if (empty($member)) {
			$member = 0;
		}
		
		$user->admin = $admin;
		$user->member = $member;
		$user->firstname = $request->input('firstname');
		$user->lastname = $request->input('lastname');
		$user->email = $request->input('email');
		$user->save();
		
        $user->employee->start = $request->input('start');
        $user->employee->end = $request->input('end');
        $user->employee->vacations_contract = $request->input('vacations_contract');
        $user->employee->vacations_extra = $request->input('vacations_extra');
        $user->employee->save();

        if ($user) {   
            return redirect()->route('admin:users:list')->with('success', 'User „'.$user->firstname.' '.$user->lastname.'“ was updated!');
        } 
        
        return redirect()->route('admin:users:list')->with('error', 'Could not update user!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	User::find($id)->delete();
		return redirect()->route('admin:users:list');
    }
}

<?php

namespace App\Http\Controllers\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\Request as AwayRequest;
use App\Models\User;
use App\Models\Holiday;
use App\Models\Calendar;

use App\Team\Repositories\CalendarManage;
use App\Mail\VacationApprove;
use App\Mail\VacationReject;
use App\Http\Requests\CalendarSave;

use Illuminate\Support\Facades\Mail;

class RequestsTeamController extends Controller
{
    /**
     * Show the application dashboard for team admin
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {   
        $users = User::query();
        // $requests = AwayRequest::pending()->get();
        $results = $users->with(['requests' => function($query) {
            $query->whereNull('approved');
                // $query->orderByRaw('date_from ASC, approved DESC');
        }])
        ->orderBy('id','ASC')
        ->get();
        return view('layouts.team.requests', [ 'users' => $results ]);

    }

    /**
     * Update the request
     *
     * @return \Illuminate\Http\Response
     */
    public function approve($id,$approved_note=null) {   

        $request = AwayRequest::find($id);
        if ($request) {  
            
            $user = Auth::user();
 
            $request->approved = true;
            $request->approved_by = $user->id;
            $request->approved_date = date('Y-m-d');
            $request->approved_note = $approved_note;
            $request->save();

            $start = new \DateTime($request->date_from);
            

            if ($request->date_to == null) {
                $end = new \DateTime($request->date_from);
            } else {
                $end = new \DateTime($request->date_to);
            }

            $startDate = $start->format('Y-m-d');
            $endDate = $end->format('Y-m-d');
            
            // add to calendar
            $holidays = Holiday::period($startDate,$endDate)->get();
            $holidays->pluck('date');

            if (count($holidays)>0) {
                foreach($holidays as $holiday) {
                    $currentHoliday = $holiday->date;
                    for($i = $start; $i <= $end; $i->modify('+1 day')){
                        if ($holiday->date == $i->format("Y-m-d") || $i->format("N") >= 6) {
                            // echo $i->format("Y-m-d")." is holiday or saturday/sunday";
                        } else {
                            $task = Calendar::updateOrCreate(
                                ['user_id' => $request->user_id, 'date' => $i->format("Y-m-d") ],
                                [
                                'date' => $i->format("Y-m-d"),
                                'user_id' => $request->user_id,
                                'project_id' => null,
                                'client_id' => null,       
                                'length' => $request->length,
                                'away' => $request->requested,
                                'comment' => $request->comment,
                            ]);
                            // echo $i->format("Y-m-d")."is not holiday";
                        }
                        
                    }
                }
            }
            else {
                for($i = $start; $i <= $end; $i->modify('+1 day')){
                    if ($i->format("N") >= 6) {
                        // echo $i->format("Y-m-d")."is saturday/sunday";
                    } else {
                        $task = Calendar::updateOrCreate(
                            ['user_id' => $request->user_id, 'date' => $i->format("Y-m-d") ],
                            [
                            'date' => $i->format("Y-m-d"),
                            'user_id' => $request->user_id,
                            'project_id' => null,
                            'client_id' => null,       
                            'length' => $request->length,
                            'away' => $request->requested,
                            'comment' => $request->comment,
                        ]);
                        // echo $i->format("Y-m-d")."is not holiday";
                    }
                    
                }
            }
            
            // die;
            // send mail
            $requestedByUser = User::find($request->user_id);
            $email = $requestedByUser->email;
            
            Mail::to($email)->send(new VacationApprove($request));

            return redirect()->route('admin:requests:list')->with('success', 'Successfully approved!');
        }
        return redirect()->route('admin:requests:list')->with('error', 'No such request!');
        
    }

    /**
     * Update the request
     *
     * @return \Illuminate\Http\Response
     */
    public function reject($id,$approved_note=null) {  

        $request = AwayRequest::find($id);
        if ($request) {  
 
            $user = Auth::user();
            $request->approved = false;
            $request->approved_by = $user->id;
            $request->approved_date = date('Y-m-d');
            $request->approved_note = $approved_note;
            $request->save();

            // send mail
            $requestedByUser = User::find($request->user_id);
            $email = $requestedByUser->email;
            
            Mail::to($email)->send(new VacationReject($request));

            return redirect()->route('admin:requests:list')->with('success', 'Successfully rejected!');
        }
        return redirect()->route('admin:requests:list')->with('error', 'No such request!');
        
    }
}
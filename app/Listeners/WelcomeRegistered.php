<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Illuminate\Auth\Events\Registered;

use App\Mail\WelcomeWithoutPass;

class WelcomeRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        // send welcome email!
        Mail::to($event->user->email)->send(new WelcomeWithoutPass($event->user));
    }
}

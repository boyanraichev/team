<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    const CREATED_AT = 'added_on';
    const UPDATED_AT = null;
    
    /*
     * The table name
     *
     * @var string
     */
    protected $table = 'requests';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_from', 'date_to', 'length', 'user_id', 'requested', 'comment', 'approved', 'approved_by', 'approved_date'
    ];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
	public $timestamps = true;
	
	/**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 20;
    
    /**
     * Casting
     *
     * @var array
     */
    protected $casts = [
	    'approved' => 'boolean',
	];
    
    /**
     * Get the project for this calendar.
     */
    public function approved()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    /**
     * Get the project for this calendar.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }  
    
    /**
     * Shows pending only
     *
     */
    public function scopePending($query)
    {
        return $query->whereNull('approved');
    }

    /**
     * Shows active only
     *
     */
    public function scopeDateRange($query,$start,$end)
    {
        return $query->where('date_from','<=',$end)->where(function ($query) use ($start, $end) {
            $query->where(function ($query) use ($start, $end) {
                $query->whereNull('date_to')->where('date_from','>=',$start);
            })->orWhere(function ($query) use ($start, $end) {
                $query->whereNotNull('date_to')->where('date_to','>=',$start);
            });
        });
    }  

}

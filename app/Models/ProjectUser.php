<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'status', 'description', 'type', 'budgeted', 'deadline',
    ];
    
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
	public $timestamps = false;
	
	/**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 20;
    
    /**
     * The table name
     *
     * @var string
     */
	protected $table = 'project_data';
    
    /**
	 * The primary key for the model.
	 *
	 * @var string
	 */    
    public $primaryKey = 'project_id';
    	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	

    /**
     * Get the project
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project','project_id','id');
    }
             
}

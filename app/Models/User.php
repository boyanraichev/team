<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'admin', 'member',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
    * Casting
    *
    * @var array
    */
    protected $casts = [
	    'admin' => 'boolean',
	    'member' => 'boolean',	    
	]; 
	
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
	public $timestamps = true;
	
	/**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 20;
	
	public function getFullNameAttribute() {
		$name = '';
		if (!empty($this->firstname)) {
			$name .= $this->firstname;
		}
		if (!empty($this->firstname)) {
			if (!empty($name)) {
				$name .= ' ';
			}
			$name .= $this->lastname;
		}
		return $name;
	}
	
    /**
    * Get the user user employee.
    */
    public function employee()
    {
        return $this->hasOne('App\Models\Employee');
    }


	/**
    * Get the user calendar.
    */
    public function calendars()
    {
        return $this->hasMany('App\Models\Calendar');
    }
    
    /**
    * Get the user projects
    */
    public function users()
    {
        return $this->belongsToMany('App\Models\Project','project_user')->withPivot('role_id');
    }  

    /**
    * Get the user request.
    */
    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }

    /**
    * Get the user calendar.
    */
    public function approvals()
    {
        return $this->hasMany('App\Models\Request', 'approved_by', 'id');
    }
    
    /**
     * Filter team members
     *
     */
    public function scopeIsTeam($query)
    {
        return $query->where('member',true);
    }

	/**
     * Get team members, ordered by first name
     *
     */
    public function scopeGetTeam($query,$user)
    {
        return $query->where('member',true)->orderByRaw('id = ? desc',[$user->id])->orderBy('firstname','desc');
	}
	
	/**
     * Get team members, ordered by first name
     *
     */
    public function scopeIsAdmin($query)
    {
        return $query->where('admin',true);
    }
    
        
}

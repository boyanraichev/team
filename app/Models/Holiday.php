<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'holidays';
        
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
    ];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
	public $timestamps = false;
	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	/**
	 * Indicates the primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'date';	
	
	/**
	 * Indicates the primary key type
	 *
	 * @var string
	 */
	protected $keyType = 'date';	
	
    /**
     * Scope a query for within date period
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePeriod($query,$start,$end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end)->orderBy('date','asc');
    }	
}

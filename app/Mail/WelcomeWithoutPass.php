<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;

class WelcomeWithoutPass extends Mailable
{
    use Queueable, SerializesModels;

	/**
     * The password unhashed.
     *
     * @var obj
     */
    public $user;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('emails.welcome_no_pass')
        			->subject('Welcome to the team');
    }
}

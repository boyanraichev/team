<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VacationReporting extends Mailable
{
    use Queueable, SerializesModels;

    public $users;
    public $start;
    public $end;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($users)
    {
        $this->users = $users['users'];
        $this->start = $users['start'];
        $this->end = $users['end'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Vacations')
                    ->view('emails.vacation_report')
                    ->with('users',$this->users)
                    ->with('start', $this->start)
                    ->with('end', $this->end);
    }   
}

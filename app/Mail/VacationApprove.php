<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Request as AwayRequest;

class VacationApprove extends Mailable
{
    use Queueable, SerializesModels;

    public $awayRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AwayRequest $awayRequest)
    {
        $this->awayRequest = $awayRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Vacation Approve')
                    ->view('emails.vacation_approve')
                    ->with('awayRequest', $this->awayRequest);
    }
}

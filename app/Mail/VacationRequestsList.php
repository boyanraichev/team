<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VacationRequestsList extends Mailable
{
    use Queueable, SerializesModels;

    public $requests;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($requests)
    {
        $this->requests = $requests['requests'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Vacation Requests List')
                    ->view('emails.vacation_requests')
                    ->with('requests',$this->requests);
    }
}

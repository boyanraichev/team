<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Request as AwayRequest;

class VacationReject extends Mailable
{
    use Queueable, SerializesModels;

    public $awayRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AwayRequest $awayRequest)
    {
        $this->awayRequest = $awayRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Vacation Reject')
                    ->view('emails.vacation_reject')
                    ->with('awayRequest', $this->awayRequest);
    }
}

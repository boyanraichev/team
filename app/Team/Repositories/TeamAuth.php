<?php

namespace App\Team\Repositories;

use App\Models\User;
use Session;

class TeamAuth {
	
	public static function checkAuthority(User $user) {
		
		$authorise = false;
		
    	if ( ! Session::get('team_admin') ) {	

		    if (!empty($user->admin)) {
		    	$authorise = true;
			    Session::put('team_admin', true);
		    }
		    
	    } else {
		    
		    $authorise = true;
		    
	    }

	    return $authorise;
	    
	}
		
}
<?php

namespace App\Team\Repositories;

use App\Models\User;
use App\Models\Employee;
use App\Models\Vacation;
use App\Events\RegisteredByOther;
use Illuminate\Auth\Events\Registered;
use Carbon\Carbon;



class UserRegister {

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

	public static function register($request,$adminRegistrar=false) {
        

		if ($adminRegistrar) {
			$password = bin2hex(openssl_random_pseudo_bytes(4));
		} else {
			$password = $request->input('password');
		}
		
		$data = [
			'email' => $request->input('email'),
			'password' => $password,
			'firstname' => $request->input('firstname'),
			'lastname' => $request->input('lastname'),
			'admin' => $request->input('admin'),
			'member' => $request->input('member'),			
		];
		
		if ($adminRegistrar) {
			$data['start'] = $request->input('start');
			$data['vacations_contract'] = $request->input('vacations_contract');
			$data['vacations_extra'] = $request->input('vacations_extra');
		}
		
		$user = self::create($data,$adminRegistrar);
		
		return $user;
    	
	}
	
	
	public static function create(array $data, $adminRegistrar = false) {
		
		if (!$adminRegistrar OR empty($data['admin'])) {
			$data['admin'] = 0;
		}
		if (empty($data['member'])) {
			$data['member'] = 1;
		}
		
		$user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'admin' => $data['admin'],   
            'member' => $data['member'],                
        ]);
        
        if ($user) {
        
        	if ($adminRegistrar) {

	        	event(new RegisteredByOther($user, $data['password']));
	        	
                $employee = Employee::create([
                    'start' => $data['start'],
                    'user_id' =>$user->id,
                    'vacations_contract' => $data['vacations_contract'],
                    'vacations_extra' => $data['vacations_extra'],
                ]);           

        	} else {
	        	
// 	        	event(new Registered($user));        		
	        	
                $employee = Employee::create([
                    'user_id' =>$user->id,
                ]);
        		
        	}
        	
        	// add referrals	        
			return $user;
    	} 
    	
    	return false;
		
	}
}

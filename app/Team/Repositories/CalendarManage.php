<?php

namespace App\Team\Repositories;

use App\Models\User;
use App\Models\Calendar;
use App\Models\Vacation;
use App\Models\Request as AwayRequest;

class CalendarManage {
	
	public static function getVacations($start=null,$end=null,$id='all') {

        if (!empty($start)) {
            $dateStart = new \DateTime($start);
		} else {
			$dateStart = new \DateTime('today');
			$dateStart->modify( 'first day of previous month' );
        }
        if (!empty($end)) {
            $dateEnd = new \DateTime($end);
		} else {
			$dateEnd = new \DateTime('today');
			$dateEnd->modify( 'last day of previous month' );
        }
        
        $users = User::query();
        if ($id!='all') {
            $users->where('id',$id);
        }
        
        $results = $users->with(['calendars' => function($query) use ($dateStart,$dateEnd) {
                $query->where('date', '>=', $dateStart->format('Y-m-d'))
                    ->where('date', '<=', $dateEnd->format('Y-m-d'))
                    ->whereIn('away',['vacation','sick'])
                    ->where('length','>',0)
                    ->orderBy('date','ASC');
            }])
            ->orderBy('id','ASC')
            ->get();

        return ['users'=>$results, 'start' => $dateStart->format('Y-m-d'), 'end' => $dateEnd->format('Y-m-d')];

    }

    public static function getRequestsVacations() {
        // $users = User::query();
        // // $requests = AwayRequest::pending()->get();
        // $results = $users->with(['requests' => function($query) {
        //     $query->whereNull('approved');
        // }])
        // ->orderBy('id','ASC')
        // ->get();
        $requests = AwayRequest::select()->whereNull('approved')->get();
        return ['requests'=>$requests];
    }
	
}
## Team Resource Planner

This is a web application for team resource planning.


Commmands reporting:

Vacations reporting

default:
(previous month - from 2018-03-01 - 2018-03-31) sending emails to config/team.php 'reporting_emails' => ['boris.apostolov@lpg.bg', 'julien.romagnoli@akktis.com'], )
cron is set every 1th of month at 13:00 to send reporting email;
php artisan report:monthly

custom period and email:
$ php artisan report:monthly --start=2018-03-01 --end=2018-04-01 --email=julien.romagnoli@akktis.com
http://teamakktis.develop/admin/reports/vacations/2018-04-04/2018-04-11

Vacation requests list to approve

default:
php artisan requests:list
http://teamakktis.develop/admin/requests

custom email:
php artisan requests:list --email --email=julien.romagnoli@akktis.com
http://teamakktis.develop/admin/requests

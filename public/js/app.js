var spinner = '<div class="spinner"><i class="spinner-icon"></i></div>';

var crsf = document.querySelector('meta[name="csrf-token"]');
if(crsf) {
	crsf = crsf.getAttribute("content");
}
		
var calendar = {
	
	ajaxurl: domain + '/calendar/',
	
	is_admin: false,
	
	div: null,
	
    init: function() {
		this.pikaday();
		this.modalHooks();
		this.load();
        this.calendarMoveListener();
        this.calendarMoveToDate();
        this.dragable();
		this.dropable();
    },
    
    showAlert: function(msg) {
	    if (!msg) {
		    msg = 'There was an error';
	    }
	    tat.modal('modal-error','<div class="alert-small error center">'+msg+'</div><div class="center"><button class="button js-modal-close">OK</button></div>',null);
	},
	
	showSuccess: function(msg) {
	    if (!msg) {
		    msg = 'Saved!';
	    }
	    tat.modal('modal-success','<div class="alert-small success center">'+msg+'</div><div class="center"><button class="button js-modal-close">OK</button></div>',null);
	},
	
	pikaday: function() {
		let datepickers = document.querySelectorAll('.pikaday');
		if (datepickers) {
			Array.from(datepickers).forEach(datepicker => {
			new Pikaday({
				field: datepicker,
				format: 'YYYY-MM-DD',
				toString: function(date, format) {
					let year = date.getFullYear().toString();
					let month = (date.getMonth()+1).toString();
					if (month.length==1) {
						month = '0'+month;
					}
					let day = date.getDate().toString() ;
					if (day.length==1) {
						day = '0'+day;
					}
					return year+'-'+month+'-'+day;
				},
				parse: function(dateString, format) {
					let parts = dateString.split('-');
					let year = parseInt(parts[0], 10);
					let month = parseInt(parts[1] - 1, 10);
					let day = parseInt(parts[2], 10);
					return new Date(year, month, day);
				},
			});
			});
		}
	},

	modalHooks: function() {
		tat.modalHooks.push(
			{
				'id': 'request-away',
				'hook': function(i,elementClicked) {
					calendar.pikaday();
					let away = elementClicked.dataset.away;
					let form = document.querySelector('.modal #request-away-form'); 
					if (away) {
						var h = form.querySelector('#request-for');
						if (h) {
							h.textContent = away;
						}
						var field = form.querySelector('#requested');
						if (field) {
							field.value = away;
						}
						
					}
					let btn = form.querySelector('button');
					if (btn) {
						btn.addEventListener('click',calendar.requestAddForm);
					}
				}
			},
			{
				'id': 'slot-settings',
				'hook': function(i,elementClicked) {
					var away = elementClicked.dataset.away;
					// calendar.pikaday();
					// if (away) {
					// 	var field = document.getElementById('request-for');
					// 	if (field) {
					// 		field.value = away;
					// 	}
					// 	var field = document.getElementById('request-for-name');
					// 	if (field) {
					// 		field.innerHTML = away;
					// 	}
					// 	var field = document.getElementById('request-for-type');
					// 	if (field) {
					// 		var type = "Request for ";
					// 		if(calendar.is_admin) {
					// 			type = "Add Calendar ";
					// 		}
					// 		field.innerHTML = type;
					// 	}
						
					// 	var field = document.getElementById('request-user_id');
					// 	var field_user = document.getElementById('user_id');
					// 	if (field) {
					// 		field.value = field_user.val;
					// 	}
					// }
	
				}
			}
		);
	},

    load: function() {
	    calendar.div = document.getElementById('calendar');
    	if (calendar.div) {
			if(calendar.div.dataset.admin) {
				calendar.is_admin = true;
			}
		    let start = calendar.div.dataset.start;
	    	let length = calendar.div.dataset.length;
	        calendar.getCalendar(start,length);
    	    window.onkeyup = calendar.keyMoveListener;
	    };
    },
    
    keyMoveListener: function(e) {
	    var start = calendar.div.dataset.start;
    	var length = calendar.div.dataset.length;
		var key = e.keyCode ? e.keyCode : e.which;
		if (key == 39) {
			calendar.getCalendar(start,length,'right');
   		} else if (key == 37) {
   			calendar.getCalendar(start,length,'left');
   		}
    },
    
    calendarMoveListener: function() {
	    let moves = document.querySelectorAll('.js-move-calendar');
	    if (moves){
		    Array.from(moves).forEach(move => {
			    move.addEventListener('click', function(e){				    
			        e.preventDefault();
				    let start = calendar.div.dataset.start;
			    	let length = calendar.div.dataset.length;
			    	let move = this.dataset.move;
			        calendar.getCalendar(start,length,move);
				});
		    })
	    }
    },
    
    calendarMoveToDate: function() {
		let cal = document.querySelector('.js-move-to');
		if (cal) {
			cal.addEventListener('click', function(e){
				e.preventDefault();
				if (calendar.calPicker) {
					calendar.calPicker.destroy();
				}
				calendar.calPicker = new Pikaday({
					format: 'YYYY-MM-DD',
					firstDay: 1,
					yearRange: 1,
					trigger: cal,
					defaultDate: calendar.div.dataset.start,
					onSelect: function(date) {
						calendar.getCalendar(calendar.calPicker.toString('YYYY-MM-DD'),calendar.div.dataset.length);
						calendar.calPicker.destroy();
					}		
				});
				cal.parentNode.insertBefore(calendar.calPicker.el, cal.nextSibling);
			});
		}
	},
    
    dragable: function() {
	    let dragables = document.querySelectorAll('.dragable');
	    if (dragables) {
		    Array.from(dragables).forEach(dragable=>{
			    dragable.addEventListener('dragstart',function(event){
				    event.dataTransfer.setData("text",this.id);
				   	event.dataTransfer.dropEffect = 'move';
				});
			});
	    };
    },
    
    dropable: function() {
	    let dropables = document.querySelectorAll('.dropable');
	    if (dropables) {
		    Array.from(dropables).forEach(dropable=>{
			    dropable.addEventListener('dragenter',calendar.dragenter);
			    dropable.addEventListener('dragover',calendar.dragover);
			    dropable.addEventListener('drop',calendar.drop); 
			});
		}
    },
    
    dragenter: function(e) {
		this.classList.add('lightgray');
		this.addEventListener('dragleave',function(e){
			this.classList.remove('lightgray');
		});
	},
	
	dragover: function(e) {
	    e.preventDefault();
	},
	
	drop: function(e){
	    e.preventDefault();
	    var slot = this;
	    slot.classList.remove('lightgray');
	    var taskID = event.dataTransfer.getData("text");
	    var data = document.getElementById(taskID);
	    var project_id = data.dataset.project;
		var date = slot.dataset.date;
		var date_comment = "";
	    var user_id = slot.dataset.user;
	    var busy = parseFloat(slot.dataset.busy); 
	    var length = '1.0';
	    var tasks = [];
	    var slotChildren = slot.querySelectorAll('.calendar-project');
	    if (slotChildren) { 
		    Array.from(slotChildren).forEach(child=>{
			    tasks.push(child.dataset.id);
		    })
	    }
		if (project_id > 0) {
			if (busy==1 || tasks.length > 1) {
				calendar.showAlert('This day is already full');
			    calendar.slotBusy(slot);
		    } else {
				if (tasks.length == '1.0') {
				    length = '0.5';
				    busy = 1;
				    if (team!==undefined) {
					    team.taskEdit(tasks[0],0.5);
					}
			    }
			    if (team!==undefined) {
 				    team.taskAdd(user_id,date,project_id,length,busy,slot);
 				    let savedTask = data.cloneNode(true);
 				    savedTask.id = null;
 					slot.append(savedTask);
			    }
			}
		} else {
			if (calendar.is_admin) {
				if (tasks.length > 0) {
					calendar.showAlert('You have to delete the saved tasks first!');
				} else {
					let away = data.dataset.away;
					let comment = "";
					team.awayAdd(date,user_id,away,slot,length,comment);
					let savedTask = data.cloneNode(true);
					savedTask.id = null;
					slot.append(savedTask);
				}
			} else {
				var params = {
					date_from: date,
					date_to: date,
					requested: data.dataset.away,
					comment: null
				}
				calendar.requestAdd(params);
			}
		}	
    },
    
    slotBusy: function(slot) {
		slot.dataset.busy = 1;
	    slot.classList.remove('dropable');
	    calendar.dropable();  
    },
    
    slotFree: function(slot) {
		slot.dataset.busy = 0;
	    slot.classList.add('dropable');
	    calendar.dropable();  
    },
    
    getCalendar: function(start,length,move) {
	    calendar.div.insertAdjacentHTML('beforeend',spinner);
	    if (move != undefined) {
		    var url = calendar.ajaxurl+start+'/'+length+'/'+move;
	    } else {
		    var url = calendar.ajaxurl+start+'/'+length;
	    }
	    axios.get(url)
	        .then(function (data) {
		        calendar.div.innerHTML = data.data.html;
			    calendar.div.dataset.start = data.data.start;
			    calendar.dropable();
			    team.listeners();
            })
            .catch(function (error) {
	            let msg = 'Could not load the calendar.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                } else {
                    msg = error;
                }
                console.log(error);
                calendar.showAlert(msg);
            });
    },
    
    requestAddForm: function(e) {
	    var form = document.querySelector('.modal #request-away-form');
		if (form.checkValidity()) {
			e.preventDefault();

			var formData = new FormData(form);
			
			if (calendar.is_admin) {

				calendar.div.insertAdjacentHTML('beforeend',spinner);
				tat.modalClose();
							
				var dates = [];
				var start = moment(formData.get('date_from')).startOf('day');
				var end = moment(formData.get('date_to')).startOf('day');
				var now = start, dates = [];
				var away = formData.get('requested');
				var user_id = formData.get('user_id');
				var comment = formData.get('comment');
				let half = formData.get('half');
				let length = half == '1' ? 0.5 : 1;
      
				while (now.isSameOrBefore(end)) {
					dates.push(now.format('YYYY-MM-DD'));
					var currentDay = now.format('YYYY-MM-DD');
					var slot = document.querySelector('div.calendar-slot[data-date="'+currentDay+'"]');
					team.awayAdd(currentDay,user_id,away,slot,length,comment);
					now.add(1, 'days');
				}
				
				setTimeout(function(){
					calendar.load();
				}, 1000)
				
				
			} else {
				calendar.requestAdd(formData);
			}
			
		}
    },
    
	requestAdd: function(formData,slot) {
		axios.post(calendar.ajaxurl+'request/add',formData)
	        .then(function (data) {
		        if (data.data.result) {
					calendar.load();
					calendar.showSuccess(data.data.message);
				} else {
				    calendar.showAlert(data.data.message);
			    }
            })
            .catch(function (error) {
	            let msg = 'Could not send request.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                }
                console.log(error);
                if (slot) {
	                let task = slot.querySelector('.calendar-project[data-away="'+requested+'"]');
				    if (task) { task.remove(); }
				}
			    calendar.showAlert(msg);
            });
	},


};
calendar.init();

// admin 
var team = {
	
	ajaxurl: domain + '/admin/ajax/',
	
	xhr: null,
	
    init: function() {
        this.getClientByName();
        this.modalHooks();
    },
    
    listeners: function() {
		this.taskDetailsListener();
        this.taskDeleteListener();
    },
    
    modalHooks: function() {
	    tat.modalHooks.push(
			{
				id: 'new-project-modal',
				hook: function(i,elementClicked) {
					calendar.pikaday();
					team.getClientByName();
				}
			}
		);
    },
    
    taskDetailsListener: function() {
	    let cal = document.getElementById('calendar');
	    let tasks = cal.querySelectorAll('.calendar-project');
	    if(tasks) {
		    Array.from(tasks).forEach(task=>{
			   task.addEventListener('click',team.taskDetails); 
		    });
	    }
    },
    
    taskDetails: function(e) {
		let content = document.createElement('div');
		let project = document.createElement('div');
		project.style.backgroundColor = this.style.backgroundColor;
		project.classList.add('padding-1-2','tat-c','calendar-project');
		project.append(this.querySelector('.project-client').cloneNode(true));
		project.append(this.querySelector('.project-name').cloneNode(true));
		content.append(project);
// 		let form = document.getElementById('edit-task-form');
		tat.modal('task-details',content,this);
    },
    
    taskDeleteListener: function(){
	    let dels = document.querySelectorAll('.js-del-task');
	    if (dels) {
		    Array.from(dels).forEach(del=>{
			    del.addEventListener('click', team.taskDeleteEv);
		    });
		}
    },
    
    taskDeleteEv: function(e) {
	    e.stopPropagation();
	    e.preventDefault();
		let task = this.closest('.calendar-project');
		if (task.dataset.project > 0) {
    		team.taskDelete(task.dataset.id);	    		
		} else {
    		team.awayDelete(task.dataset.id);	    		
		}
    },
    
    getClientByName: function() {
	    field = document.getElementById('client_search');
	    if (field) {
		    
		    new autoComplete({
			    selector: field,
			    minChars: 3,
			    source: function(term, suggest){
			        let clientid = document.getElementById('client_id-auto');
			        if(clientid){ clientid.value = ''; }
			        
			        try { team.xhr.abort(); } catch(e){}
			        team.xhr = axios.get(team.ajaxurl+'clients/search?term='+ encodeURIComponent(term))
						.then(function (data) {
							term = term.toLowerCase();
			                var choices = data.data;
			                var suggestions = [];
			                for (i=0;i<choices.length;i++){
			                    if (~(choices[i].value).toLowerCase().indexOf(term)){ suggestions.push(choices[i]); }
			                }
			                suggest(suggestions);
							})
						.catch(function (error) {
							console.log(error);
							});
			    },
			    renderItem: function (item, search){
			        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
			        return '<div class="autocomplete-suggestion" data-val="'+item.value+'" data-client-id="'+item.id+'" data-client-name="'+item.value+'" >'+item.value.replace(re, "<b>$1</b>")+'</div>';
			    },
			    onSelect: function(e, term, item){
			        let clientid = document.getElementById('client_id-auto');
			        if(clientid){
			            clientid.value = item.getAttribute('data-client-id');
				 	}
					let clientname = document.getElementById('client_name-auto');
			        if(clientname){
			            clientname.value = item.getAttribute('data-client-name');
				 	}
			    }
			});
		}
    },
    
    taskAdd: function(user_id,date,project_id,length,busy,slot) {
	    let formData = {
		    'date': date,
		    'user_id': user_id,
		    'project_id': project_id,
		    'length': length
	    }
	    axios.post(team.ajaxurl+'calendar/add',formData)
	        .then(function (data) {
		        if (data.data.result) {
				    if (busy) {
					    calendar.slotBusy(slot);
				    }
				    let task = slot.querySelector('.calendar-project[data-project="'+project_id+'"]');
				    if (task) {
					    task.dataset.length = length;
					    task.dataset.id = data.data.id;
					}
	    		    team.listeners(); 
			    } else{
				    let task = slot.querySelector('.calendar-project[data-project="'+project_id+'"]');
				    if (task) { task.remove(); }
				    calendar.showAlert(data.data.message);
			    }
            })
            .catch(function (error) {
	            let msg = 'Could not save task.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                }
                console.log(error);
                let task = slot.querySelector('.calendar-project[data-project="'+project_id+'"]');
			    if (task) { task.remove(); }
			    calendar.showAlert(msg);
            });
            
    },

    taskEdit: function(id,length) {
	    let formData = {
		    'length': length,
		    '_method': 'PUT'
	    }
	    axios.post(team.ajaxurl+'calendar/edit/'+id,formData)
	        .then(function (data) {
		        if (data.data.result) {
				    el = document.querySelector('.calendar-project[data-id="'+id+'"]');
				    el.dataset.length = length;
				} else {
					calendar.showAlert(data.data.message);
				}
            })
            .catch(function (error) {
	            let msg = 'Could not save task.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                }
                console.log(error);
			    calendar.showAlert(msg);
            });
    },
    
    taskDelete: function(id) {
	    let formData = {
		    '_method': 'DELETE'
	    }
	    axios.post(team.ajaxurl+'calendar/del/'+id,formData)
	        .then(function (data) {
		        if(data.data.result){		
				    el = document.querySelector('.calendar-project[data-id="'+id+'"]');
				    var slot = el.closest('.calendar-slot');
				    el.remove();
				    calendar.slotFree(slot);
		    	} else {
			    	calendar.showAlert(data.data.message);
		    	}
            })
            .catch(function (error) {
	            let msg = 'Could not delete task.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                }
                console.log(error);
			    calendar.showAlert(msg);
            });
	},
	
    awayAdd: function(date,user_id,away,slot,length,comment) {
	    
	    if (length==undefined) {
		    length = 1;
	    }
	    let formData = {
		    'date': date,
		    'user_id': user_id,
		    'length': length,
			'away': away,
			'comment': comment
	    }
	    axios.post(team.ajaxurl+'calendar/away/add',formData)
	        .then(function (data) {
			    let task = slot.querySelector('.vacation[data-away="'+away+'"]');
		        if (data.data.result) {
					if (task) { task.dataset.id = data.data.id; }
				    calendar.slotBusy(slot);
					team.listeners();
			    } else {
				    if (task) { task.remove(); }
				    calendar.showAlert(data.data.message);
			    }
            })
            .catch(function (error) {
	            let msg = 'Could not save away day.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                }
                console.log(error);
			    let task = slot.querySelector('.vacation[data-away="'+away+'"]');
			    if (task) { task.remove(); }
			    calendar.showAlert(msg);
            });
	},
	
    awayDelete: function(id) {
	    let formData = {
		    '_method': 'DELETE'
	    }
	    axios.post(team.ajaxurl+'calendar/away/del/'+id,formData)
	        .then(function (data) {
		        if (data.data.result){
				    var el = document.querySelector('.calendar-project[data-id="'+id+'"]');
				    var slot = el.closest('.calendar-slot');
				    el.remove();
				    calendar.slotFree(slot);
				    
			    } else {
				    calendar.showAlert(data.data.message);
			    }
            })
            .catch(function (error) {
	            let msg = 'Could not delete away day.';
                if (error.response && error.response.data.message) {
                    msg = error.response.data.message;
                    if (error.response.data.errors) {
                        msg = error.response.data.errors[Object.keys(error.response.data.errors)[0]];
                    }
                }
                console.log(error);
			    calendar.showAlert(msg);
            });

    },

};

team.init();


var swatch = {
	init: function() {
		var colorInput = document.getElementById('color');
		let colors = document.querySelectorAll('.swatch');
		Array.from(colors).forEach(swatch => {
		    swatch.addEventListener('click',function() {
				var color = this.style.backgroundColor; 
				var hex_rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/); 
				function hex(x) {return ("0" + parseInt(x).toString(16)).slice(-2);}
				if (hex_rgb) {
					colorInput.value = hex(hex_rgb[1]) + hex(hex_rgb[2]) + hex(hex_rgb[3]);
					colorInput.style.borderColor = '#'+colorInput.value;
					colorInput.style.borderLeft = '2.5rem solid #'+colorInput.value;
				}
			});
		});
		if (colorInput) {
			colorInput.addEventListener('change',visualiseColor, false);
		}
		function visualiseColor() {
			colorInput.style.borderColor = '#'+this.value;
			this.style.borderLeft = '2.5rem solid #'+this.value;
		}
	}
}
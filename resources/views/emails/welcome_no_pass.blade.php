Hello {{ $user->first_name }},

Welcome to your team calendar on {{ config('app.url') }}.

You can login to your account with email {{ $user->email }} and the password you chose at registration.

Greetings,
Your team
Hello {{ $user->first_name }},

Welcome to your team calendar on {{ config('app.url') }}. You were registered by a fellow team member.

You can login to your account with email {{ $user->email }} and password:
{{ $password }}

Greetings,
Your team
@extends('emails.email')

@section('title', 'Vacations reject')

@section('content')

    <p><strong>Vacations rejected {{ $awayRequest->date_from }} - {{ $awayRequest->date_to }} </strong></p>

    <p>Note: {{$awayRequest->approved_note}}</p>
    
@endsection
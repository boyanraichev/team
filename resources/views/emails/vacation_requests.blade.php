@extends('emails.email')

@section('title', 'Vacations')

@section('content')
<p>You have to approve new {{count($requests)}} 
        <a href="{{ route('admin:requests:list') }}"><span class="header-team text-blue">Vacation Requests.</span></a>
    </p>
@endsection
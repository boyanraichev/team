@extends('emails.email')

@section('title', 'Vacations Approve')

@section('content')

    <p><strong> Happy to be ;) Vacations approved {{ $awayRequest->date_from }} - {{ $awayRequest->date_to }} </strong></p>

    <p>Reason: {{$awayRequest->approved_note}}</p>
    
@endsection
@extends('emails.email')

@section('title', 'Vacations')

@section('content')

    <p><strong>Vacations report for {{ $start }} - {{ $end }}</strong></p>

    <p>Vacations:</p>
    
    <table cellpadding="5">
        <thead>
            <tr>
                <td>Name</td>
                <td>Date</td>
                <td>Length</td>
                <td>Comment</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                @if (count($user->calendars) > 0) 
                    @foreach ($user->calendars as $calendar) 
                        @if ($calendar->away=='vacation') 
                            <tr>
                                <td>{{ $user->fullname }}</td>
                                <td>{{ $calendar->date }}</td>
                                <td>{{ $calendar->length }}</td>
                                <td>{{ $calendar->comment }}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>

    <p>Illnesses:</p>
    <table cellpadding="5">
        <thead>
            <tr>
                <td>Name</td>
                <td>Date</td>
                <td>Length</td>
                <td>Comment</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                @if (count($user->calendars) > 0) 
                    @foreach ($user->calendars as $calendar) 
                        @if ($calendar->away=='sick') 
                            <tr>
                                <td>{{ $user->fullname }}</td>
                                <td>{{ $calendar->date }}</td>
                                <td>{{ $calendar->length }}</td>
                                <td>{{ $calendar->comment }}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>
    
@endsection
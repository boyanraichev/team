@extends('app')

@section('title', 'Reset your password')

@section('content')

<main class="viewport tat-c tat-center tat-middle">
    	    
    <h1 class="container-xs">reset password</h1>

	<form class="container-xs white padding-1" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        @if (session('status'))
            <div class="alert success">
                <span>{{ session('status') }}</span>
            </div>
            
        @endif
		
		<label for="email">E-Mail Address</label>
        <input id="email" type="email" class="{{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

        @if ($errors->has('email'))
            <div class="alert-small error">
                {{ $errors->first('email') }}
            </div>
        @endif


		<div>
            <button type="submit" class="button">
                Send Password Reset Link
            </button>
		</div>

	</form>

</main>

@endsection

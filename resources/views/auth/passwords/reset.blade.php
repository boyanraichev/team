@extends('app')

@section('title', 'Reset your password')

@section('content')

<main class="viewport tat-c tat-center tat-middle">
    	    
    <h1 class="container-xs">reset password</h1>
    
    <form class="container-xs white padding-1" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}
        
        <input type="hidden" name="token" value="{{ $token }}">

        @if (session('status'))
            <div class="alert success">
                <span>{{ session('status') }}</span>
            </div>
            
        @endif

		
		<label for="email">E-Mail Address</label>
        <input id="email" type="email" class="{{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

        @if ($errors->has('email'))
            <div class="alert-small error">
                {{ $errors->first('email') }}
            </div>
        @endif
        
        <label for="password">Password</label>
        <input id="password" type="password" class="{{ $errors->has('password') ? ' has-error' : '' }}" name="password" required>

		<label for="password-confirm">Confirm Password</label>
        <input id="password-confirm" type="password" class="{{ $errors->has('password') ? ' has-error' : '' }}" name="password_confirmation" required>    
            
        @if ($errors->has('password'))
            <span class="alert-small error">
                {{ $errors->first('password') }}
            </span>
        @endif

        
		<div>
            <button type="submit" class="button">
                Reset Password
            </button>
		</div>

	</form>

</main>
                        
@endsection

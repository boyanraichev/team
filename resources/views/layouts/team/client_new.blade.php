@extends('app')

@section('title', 'New client')

@section('content')

<main class="viewport tat-c tat-center tat-middle">
    	    
    <h1 class="container-xs">New Client</h1>
	    
    <form class="container-xs white padding-1" method="POST" action="{{ route('admin:clients:create') }}">

		@if ($errors->any())
			<div class="alert error">Please, fill in all required fields.</div>
		@endif
		 
        {{ csrf_field() }}

        <label for="name">Client</label>
        <input id="name" type="text" class="{{ $errors->has('name') ? ' has-error' : '' }}" name="name" placeholder="Client Name" value="{{ old('name')? old('name') : '' }}" required autofocus>

        @if ($errors->has('name'))
            <div class="alert-small error">
                {{ $errors->first('name') }}
            </div>
        @endif
		
		<label class="checkbox" for="active">
            <input type="checkbox" name="active" id="active" value="1" {{ !empty(old('active')) ? 'checked' : '' }}> Currently active
        </label>
        
        <div>
            <button type="submit" class="button green">
                Create
            </button>
        </div>
        
    </form>
    
</main>
	

@endsection
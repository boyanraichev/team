@extends('app')

@section('title', 'Team projects')

@section('content')

<main class="viewport tat-c tat-center tat-middle">
	
	<h1 class="container-l">clients & projects</h1>
	
	<div class="container-l">
			
		<div id="projects-filters" class="tat-c tat-r-sm tat-middle-sm tat-hspace">
			
			<div class="">
				
				
			</div>
			
			<div class="tat-r-sm tat-middle">
				<a href="{{ route('admin:clients:new') }}" class="button ghost">+ client</a>
		    	<a href="{{ route('admin:projects:new') }}" class="button ghost pushleft-1-2">+ project</a>
			</div>
		
		</div>
			
		@if (session('success'))
		    <div class="alert success">
		        {{ session('success') }}
		    </div>
		@endif
	
		<div id="projects-tree" class="white tat-u-1 padding-1">
			
			All clients and projects.
						
			@if (count($clients) > 0)
						
				@foreach ($clients as $client)
					<div class="client-row tat-c tat-r-sm tat-hspace tat-bottom tat-top-sm pushup-1 @if (!$client->active) dormant @endif">
						<div class="client tat-u-1 tat-u-sm-2-5 tat-r tat-middle">
							<a href="#" class="tat-f-1-0">{{ $client->name }}</a>
							<a href="{{ route('admin:client:edit', $client->id) }}" class="tat-f-0-0"><i class="material-icons">edit</i></a>
						</div>
						
						@if (count($client->projects) > 0)
							<div class="client-projects tat-u-4-5 tat-u-sm-2-5 tat-c">
								@foreach ($client->projects as $project)	
									<div class="project tat-u-1 tat-r tat-middle green @if (!$project->active) dormant @endif @if (ColorsHelper::isDark($project->color)) darkcolor @endif"
										@if (!empty($project->color))
										style="background-color:#{{$project->color}};"
										@endif
										>
										<a href="{{ route('admin:projects:stats', $project->id)}}" class="tat-f-1-0">{{ $project->name }}</a>
										<a href="{{ route('admin:project:edit', $project->id) }}" class="tat-f-0-0"><i class="material-icons text-white">edit</i></a>
									</div>
								@endforeach
							</div>
						@endif
					</div>
				@endforeach
				
			@endif
			
		</div>
		
		
	</div>
	
</main>


@endsection

@section('scripts')
<script>
swatch.init();

</script>
@endsection
@extends('app')

@section('title', 'New project')

@section('content')

<main class="viewport tat-c tat-center tat-middle">
    	    
    <h1 class="container-xs">New Project</h1>
	    
	<div class="container-xs white">

		@if ($errors->any())
			<div class="alert error">Please, fill in all required fields.</div>
		@endif
				
			
	    <form class="container-xs white padding-1" method="POST" action="{{ route('admin:projects:create') }}">
	
	        {{ csrf_field() }}
	
	        <label for="name">Name</label>
	        <input id="name" type="text" class="{{ $errors->has('name') ? ' has-error' : '' }}" name="name" placeholder="Project Name" value="{{ old('name') }}" maxlength="30" required autofocus>
	
	        @if ($errors->has('name'))
	            <div class="alert-small error">
	                {{ $errors->first('name') }}
	            </div>
	        @endif
	        
	        <label for="client_search">Client</label>
	        <input id="client_search" type="text" name="client_search" placeholder="Find client by name" value="{{ old('client_search') }}" maxlength="30" required>
	        
			
			<label for="client_name-auto">Client Name</label>
			<input type="text" class="{{ $errors->has('client_id') ? ' has-error' : '' }}" name="client_name" id="client_name-auto" maxlength="30" value="{{ old('client_name') }}" readonly>
			<input type="hidden" name="client_id" id="client_id-auto" value="{{ old('client_id') }}">
			
	        @if ($errors->has('client_id'))
	            <div class="alert-small error">
	                {{ $errors->first('client_id') }}
	            </div>
	        @endif
	
			<label for="url">URL</label>
	        <input id="url" type="text" class="{{ $errors->has('url') ? ' has-error' : '' }}" name="url" placeholder="Project URL" value="{{ old('url') }}" maxlength="100">
	
	        @if ($errors->has('url'))
	            <div class="alert-small error">
	                {{ $errors->first('url') }}
	            </div>
	        @endif
	        
	        <label for="description">Description</label>
	        <textarea id="description" class="{{ $errors->has('description') ? ' has-error' : '' }}" name="description" placeholder="Project description" maxlength="300">{{ old('description') }}</textarea>
	
	        @if ($errors->has('description'))
	            <div class="alert-small error">
	                {{ $errors->first('description') }}
	            </div>
	        @endif
			        
	        <label for="status">Status</label>
	        <textarea id="status" class="{{ $errors->has('status') ? ' has-error' : '' }}" name="status" placeholder="Project current status" maxlength="300">{{ old('status') }}</textarea>
	
	        @if ($errors->has('description'))
	            <div class="alert-small error">
	                {{ $errors->first('description') }}
	            </div>
	        @endif
	
	        <label for="budgeted">Budgeted days</label>
	        <input name="budgeted" placeholder="Budgeted days of work" class="{{ $errors->has('budgeted') ? ' has-error' : '' }}" type="number" value="{{ old('budgeted') }}">
	
	        @if ($errors->has('budgeted'))
	            <div class="alert-small error">
	                {{ $errors->first('budgeted') }}
	            </div>
	        @endif
	
	        <label for="deadline">Deadline</label>
	        <input name="deadline" placeholder="Deadline" class="pikaday {{ $errors->has('deadline') ? ' has-error' : '' }}" type="text" id="datepickerend" value="{{ old('deadline') }}">
	
	        @if ($errors->has('deadline'))
	            <div class="alert-small error">
	                {{ $errors->first('deadline') }}
	            </div>
	        @endif
	
	        <label for="color">Color</label>
	        <input id="color" type="text" name="color" placeholder="Project color" value="{{ old('color') }}" maxlength="6" 
	        	@if(!empty(old('color')))
	        		style="border-bottom: 6px solid #{{ old('color') }};"
	        	@endif
	        	>
			
			{{ ColorsHelper::display() }}
			
			<label class="checkbox" for="active">
	            <input type="checkbox" name="active" id="active" value="1" {{ !empty(old('active')) ? 'checked' : '' }}> Currently active
	        </label>
			
	        <div>
	            <button type="submit" class="button green">
	                Edit Project
	            </button>
	        </div>
	    </form>

	</div>
	
</main>

@endsection

@section('scripts')
<script>
swatch.init();
</script>
@endsection
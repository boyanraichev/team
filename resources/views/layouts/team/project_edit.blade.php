@extends('app')

@section('title', 'Edit project')

@section('content')
<main class="viewport tat-c tat-center tat-middle">
    	    
    <h1 class="container-xs">Edit Project</h1>
	    
	<div class="container-xs white">
		
		@if ($errors->any())
			<div class="alert error">Please, fill in all required fields.</div>
		@endif
		
	    <form class="padding-1" method="POST" action="{{ route('admin:project:edit', $project->id) }}">
			
			<input type="hidden" name="_method" value="PUT">
				 
			{{ csrf_field() }}
	
	        <label for="name">Name</label>
	        <input id="name" type="text" class="{{ $errors->has('name') ? ' has-error' : '' }}" name="name" placeholder="Project Name" value="{{ old('name')? old('name') : $project->name }}" maxlength="30" required autofocus>
	
	        @if ($errors->has('name'))
	            <div class="alert-small error">
	                {{ $errors->first('name') }}
	            </div>
	        @endif
	
			<label for="url">URL</label>
	        <input id="url" type="text" class="{{ $errors->has('url') ? ' has-error' : '' }}" name="url" placeholder="Project URL" value="{{ old('url')? old('url') : $project->details->url }}" maxlength="100">
	
	        @if ($errors->has('url'))
	            <div class="alert-small error">
	                {{ $errors->first('url') }}
	            </div>
	        @endif
	        
	        <label for="description">Description</label>
	        <textarea id="description" class="{{ $errors->has('description') ? ' has-error' : '' }}" name="description" placeholder="Project description" maxlength="300">{{ old('description')? old('description') : $project->details->description }}</textarea>
	
	        @if ($errors->has('description'))
	            <div class="alert-small error">
	                {{ $errors->first('description') }}
	            </div>
	        @endif
	        
	        <label for="status">Status</label>
	        <textarea id="status" class="{{ $errors->has('status') ? ' has-error' : '' }}" name="status" placeholder="Project current status" maxlength="300">{{ old('status')? old('status') : $project->details->status }}</textarea>
	
	        @if ($errors->has('description'))
	            <div class="alert-small error">
	                {{ $errors->first('description') }}
	            </div>
	        @endif
	        
	
	        <label for="budgeted">Budgeted days</label>
	        <input name="budgeted" placeholder="Budgeted days of work" class="{{ $errors->has('budgeted') ? ' has-error' : '' }}" type="number" value="{{ old('budgeted') ? old('budgeted') : $project->details->budgeted }}">
	
	        @if ($errors->has('budgeted'))
	            <div class="alert-small error">
	                {{ $errors->first('budgeted') }}
	            </div>
	        @endif
	
	        <label for="deadline">Deadline</label>
	        <input name="deadline" placeholder="Deadline" class="pikaday {{ $errors->has('deadline') ? ' has-error' : '' }}" type="text" id="datepickerend" value="{{ old('deadline') ? old('deadline') : $project->details->deadline }}">
	
	        @if ($errors->has('deadline'))
	            <div class="alert-small error">
	                {{ $errors->first('deadline') }}
	            </div>
	        @endif
	
	        <label for="color">Color</label>
	        <input id="color" type="text" name="color" placeholder="Project color" value="{{ old('color') ? old('color') : $project->color }}" maxlength="6" style="border-color: #{{  $project->color }}; border-left: 2.5rem solid #{{  $project->color }};">
			
			{{ ColorsHelper::display() }}
			
			<label class="checkbox" for="active">
	            <input type="checkbox" name="active" id="active" value="1" {{ $project->active ? 'checked' : '' }}> Currently active
	        </label>
			
	        <div>
	            <button type="submit" class="button green">
	                Update
	            </button>
	        </div>
	    </form>
	    
	</div>
	
</main>
	

@endsection

@section('scripts')
<script>
swatch.init();
</script>
@endsection
@extends('app')

@section('title', 'calendar')

@section('content')


<main class="viewport tat-c-r tat-r-xl tat-center tat-top">
	
	<div id="calendar-bar" class="padding-1-2 white tat-f-1-0 tat-u-1 tat-u-xl-4-5">
		
		@if (session('status'))
		    <div class="alert success">
		        {{ session('status') }}
		    </div>
		@endif
		
		@if($hasRequests) 
			<div class="alert error">
				There are pending
				<a href="{{ route('admin:requests:list') }}" class="">vacation requests.</a>
			</div>
		@endif		
		
		<div id="calendar-nav" class="tat-r tat-hspace">
			<span class="js-move-calendar" data-move="left">
				<i class="material-icons" >keyboard_arrow_left</i>
			</span>
			<div class="calendar-picker">
				<a href="#" class="js-move-to">
					choose date	
				</a>
			</div>
			<span class="js-move-calendar" data-move="right">
				<i class="material-icons" >keyboard_arrow_right</i>
			</span>
		</div>
		
		
		<div id="calendar" class="js-calendar tat-r" 
			data-start="today" 
			data-length="{{ $length }}"
			@teamleader
				data-admin="1"
			@endteamleader
			>
			
			<div id="calendar-members" class="tat-f-0-0">
			
				@if (count($users) > 0)
							
					@foreach ($users as $member)
						
						<div class="calendar-member right @if ($user->id==$member->id) myself @endif">
							{{ $member->firstname }} {{ $member->lastname }}
						</div>
						
					@endforeach
					
				@endif
				
			</div>
			
			<div id="calendar-days" class="tat-f-1-1 tat-g">
				
			</div>
		
		</div>	
		
	</div>


	<div id="projects-bar" class="white padding-1-2 tat-f-0-0 tat-u-1 tat-u-xl-1-5 tat-g">
		
		@teamleader
			<h5 class="tat-u-1 text-gray">Tasks:</h5>
			
			@if (count($projects) > 0)
							
					@foreach ($projects as $project)
						
						<div id="calendar-project-{{ $project->id }}" class="calendar-project dragable tat-f-0-0 tat-r green @if (ColorsHelper::isDark($project->color)) darkcolor @endif" 
							@if (!empty($project->color)) style="background-color: #{{$project->color}}" @endif
							draggable="true" 
							data-project="{{ $project->id }}"
							data-members=""
							>
							<div class="tat-f-1-1 tat-c tat-center">
								<span class="project-client">{{ $project->client->name }}</span>
								<span class="project-name">{{ $project->name }}</span>
							</div>
							<div class="tat-f-0-0 tat-s-middle project-actions">
								<span class=""><i class="js-del-task material-icons text-white" >close</i></span>
							</div>
						</div>
						
					@endforeach
					
			@endif
		@else
			<h5 class="tat-u-1 text-gray">Day off:</h5>
		@endteamleader
		
		
		<div id="calendar-project-vacation" class="calendar-project vacation dragable tat-f-0-0 tat-r tat-middle lightgray js-modal" data-modal="request-away" data-modal-get="request-away-proto"
		 draggable="true" style="" data-length="1.0" data-away="vacation">		
			<span class="tat-f-1-1 away-name">Vacation</span>
			<div class="tat-f-0-0 tat-s-middle project-actions">
				<span class=""><i class="js-del-task material-icons" >close</i></span>
			</div>
		</div>
		
		<div id="calendar-project-sick" class="calendar-project vacation dragable tat-f-0-0 tat-r tat-middle lightgray js-modal" data-modal="request-away" data-modal-get="request-away-proto"
		draggable="true" style="" data-length="1.0" data-away="sick">		
			<span class="tat-f-1-1 away-name">Illness</span>
			<div class="tat-f-0-0 tat-s-middle project-actions">
				<span class=""><i class="js-del-task material-icons" >close</i></span>
			</div>
		</div>
		
		<div id="calendar-project-away" class="calendar-project vacation dragable tat-f-0-0 tat-r tat-middle lightgray  js-modal" data-modal="request-away" data-modal-get="request-away-proto"
		draggable="true" style="" data-length="1.0" data-away="other">		
			<span class="tat-f-1-1 away-name">Other</span>
			<div class="tat-f-0-0 tat-s-middle project-actions">
				<span class=""><i class="js-del-task material-icons" >close</i></span>
			</div>
		</div>	
		
		
		<div id="request-away-proto" class="tat-c tat-r-sm" style="display: none;">
			<form id="request-away-form" action="{{ route('user:request:add') }}" method="post">
				{{ csrf_field() }}
				
				<h3>
					@teamleader
						Add
					@else
						Request
					@endteamleader
					: <span id="request-for"></span>
				</h3>
				
				<label for="date_from">From</label>
				<input type="text" class="pikaday" name="date_from" id="date_from" placeholder="Date from" autocomplete="off" required>
				
				<label for="date_to">To</label>
				<input type="text" class="pikaday" name="date_to" id="date_to" placeholder="Date to" autocomplete="off" required>
				
				<label for="half">
					<input type="checkbox" class="" name="half" id="half" value="1"> Half day
				</label>
					
				<label for="comment">Comment</label>
				<textarea class="" name="comment" id="comment" placeholder="Additional information"></textarea>
				
				<input type="hidden" id="requested" name="requested">
				
				@teamleader
					<label for="user_id">Member</label>
					<select id="user_id" name="user_id" required>
						@if (count($users) > 0)
							@foreach ($users as $member)
								<option value="{{ $member->id }}">{{ $member->firstname }} {{ $member->lastname }}</option>
							@endforeach
						@endif
					</select>
				@endteamleader
				
				<button class="tat-u-1 tat-u-sm-a button green submit-request" >
					@teamleader
						Add
					@else
						Request
					@endteamleader
				</button>
			</form>
		</div>
		
	</div>
</main>

@endsection

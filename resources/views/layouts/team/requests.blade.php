@extends('app')

@section('title', 'Requests Vacations')

@section('content')

<main class="viewport tat-c tat-center tat-middle">
	
	<h1 class="container-l">Vacations Requests</h1>
	
	<div class="container-l white tat-g">

        @if (session('success'))
            <div class="alert success">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert error">
                {{ session('error') }}
            </div>
        @endif
			
		<div id="project-stat" class="tat-u-1 padding-1">
            <?php $hasRequests = false; ?>
            @foreach ($users as $user)

                @if (count($user->requests) > 0) 
                    <?php $hasRequests = true; ?>
                    <span class="text-darkgray">User: </span> <strong>{{ $user->fullname }}</strong> ({{ $user->email }})
                    </br>
                    @foreach ($user->requests as $request)
                        {{-- форма за аппрове --}}
                                
                        <span class="text-darkgray">Date from: </span> {{ $request->date_from }}
                        @if ($request->date_to!=null)
	                        <br/>
                            <span class="text-darkgray">Date to: </span> {{ $request->date_to }}
                        @endif
                        <br/>
                        <span class="text-darkgray">Length: </span>  @if($request->length == '1.0') Full @else Half day @endif
                        <br/>
                        <span class="text-darkgray">Requested: </span> {{ $request->requested }}
                        <br/>
                        <span class="text-darkgray">Comment: </span> {{ $request->comment }}
                        <br/>
                        {{--
                            <span class="text-darkgray">Added on: </span> {{ $request->added_on->format('Y-m-d') }}
                         @if ($request->approved!=null)
                            <span class="text-darkgray">Approved: </span>
                            @if ($request->approved==true)
                                <span class="text-green">Accepted </span>
                            @endif
                            @if ($request->approved==false)
                                <span class="text-red">Rejected </span>
                            @endif
                        @endif
                        @if ($request->approved_note!=null)
                            <span class="text-darkgray">Approved Note: </span>
                                <span class="text-green">Accepted </span>
                        @endif
                        @if ($request->approved_by!=null)
                            <span class="text-darkgray">Approved By: </span> {{ $request->approved_by }}
                        @endif --}}
                        <div class="tat-c tat-r-sm"> 
                            <form class="tat-f-0-0 white" method="POST" action="{{ route('admin:request:approve', $request->id) }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                                <button type="submit" class="tat-u-1 tat-u-sm-a button green">
                                    Approve
                                </button>
                            </form>
                            <form class="tat-f-0-0 white" method="POST" action="{{ route('admin:request:reject', $request->id) }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                                <button type="submit" class="tat-u-1 tat-u-sm-a button red">
                                    Reject
                                </button>
                            </form>  
                        </div>
                        
                    </br>
                    @endforeach
                </br>
                
                @endif

            @endforeach
            @if($hasRequests == false)
            <strong><span class="text-darkgray">Doesn't have any vacations requests.</strong>
            @endif
		     
<!--
			{{-- @foreach ($project->calendars()->groupBy('user_id') as $user) --}}
				<div class="tat-r">
					{{-- <span>Project Total Days per {{$user->firstname}}: </span>	 --}}
					{{-- {{$project->calendars->where('user_id', $user->id)->sum('length')}} --}}
				</div>
			{{-- @endforeach --}}
-->
		</div>
		
	</div>
</main>

@endsection